package com.huan.es8.dslapi;

import co.elastic.clients.elasticsearch._types.mapping.Property;
import co.elastic.clients.elasticsearch._types.mapping.TextProperty;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.huan.es8.AbstractEs8Api;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.util.Arrays;

/**
 * 前缀查询
 * 前缀查询比较耗性能。 如果需要使用，则需要加上 index_prefixes
 *
 * @author huan.fu
 * @date 2023/3/13 - 23:18
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PrefixQuery extends AbstractEs8Api {

    private static final String INDEX_NAME = "index_language";

    @Test
    public void createIndex() throws IOException {
        client.indices()
                .create(indexRequest ->
                        indexRequest.index(INDEX_NAME)
                                .mappings(mappings ->
                                        mappings.properties("language",
                                                new Property(
                                                        new TextProperty.Builder()
                                                                .analyzer("ik_max_word")
                                                                .searchAnalyzer("ik_smart")
                                                                // 如果需要用到前缀查询，最好加上 indexPrefixes，因为prefix查询特别耗性能，加上这个稍微好点
                                                                .indexPrefixes(prefix ->
                                                                        prefix.minChars(2)
                                                                                .maxChars(5))
                                                                .build()
                                                )
                                        )
                                )
                );
        bulk(INDEX_NAME, Arrays.asList(
                "{\"language\":\"Java编程语言\"}",
                "{\"language\":\"JavaScript编程语言\"}",
                "{\"language\":\"C#编程语言\"}"
        ));
    }

    @DisplayName("前缀查询")
    @Test
    public void prefixTest() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index(INDEX_NAME)
                        .query(query ->
                                query.prefix(prefix ->
                                        prefix.field("language")
                                                .value("java")
                                )
                        )
                        .size(100)
        );

        System.out.println("request: " + request);
        SearchResponse<Object> response = client.search(request, Object.class);
        System.out.println("response: " + response);
    }

    @AfterAll
    @Test
    public void after() throws IOException {
        deleteIndex(INDEX_NAME);
    }
}
