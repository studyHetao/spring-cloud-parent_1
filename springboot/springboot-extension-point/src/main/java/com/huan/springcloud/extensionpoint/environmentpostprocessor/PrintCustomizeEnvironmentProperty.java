package com.huan.springcloud.extensionpoint.environmentpostprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class PrintCustomizeEnvironmentProperty implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(PrintCustomizeEnvironmentProperty.class);

    @Value("${username}")
    private String userName;

    @Override
    public void run(ApplicationArguments args) {
        log.info("获取到的 username 的属性值为: {}", userName);
    }
}
