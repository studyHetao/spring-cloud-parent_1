package com.huan.study.sharding.controller;

import com.huan.study.sharding.entity.Customer;
import com.huan.study.sharding.mappers.CustomerMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author huan.fu 2021/5/18 - 上午10:51
 */
@RestController
public class CustomerController {

    @Resource
    private CustomerMapper customerMapper;

    @GetMapping("addCustomer")
    public String addCustomer(@RequestParam("phone") String phone, @RequestParam("address") String address) {
        int result = customerMapper.addCustomer(phone, address);
        return "添加结果: " + result;
    }

    @GetMapping("findCustomer")
    public Customer findCustomer(@RequestParam("phone") String phone) {
        return customerMapper.findCustomer(phone);
    }

    @GetMapping("findPage")
    public Map<String, Object> findPage(@RequestParam("pageIndex") int pageIndex, @RequestParam("pageSize") int pageSize,
                                        @RequestParam("id") Long id) {
        int total = customerMapper.countCustomer();
        List<Customer> records = customerMapper.findCustomerPage(pageIndex * pageSize, pageSize, id);
        Map<String, Object> result = new HashMap<>(4);
        result.put("total", total);
        result.put("records", records);
        return result;
    }
}
