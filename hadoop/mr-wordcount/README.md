1. 实现一个简单的 mapreduce 的 word count 案例。
2. 本地需要有`Hadoop`的环境，否则可能运行不起来。
3. 如何确定 mr 程序是以那种模式运行，`local` or `yarn`， 是由`mapreduce.framework.name`的值确定的，默认值是`local`
4. 在`yarn`集群上运行，需要配置如下2个参数，可以在配置文件中指定，也可以在程序代码中指定。 `mapreduce.framework.name=yarn`
   和 `yarn.resourcemanager.hostname=hadoop02`
5. `yarn`上运行
    1. 上传文件到 `hadoop 集群`
    2. 执行`hadoop jar mr-wordcount-0.0.1-SNAPSHOT.jar /data/input /data/output`命令
    3. 输出路径必须不存在，否则报错
6. `local`本地模式运行
    1. 直接运行Main方法即可
7. `job.setNumReduceTasks(2);`会导致输出的文件个数变成2个