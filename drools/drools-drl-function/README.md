# 实现功能

1、在drl文件中自定义函数，然后在 then 之后进行调用。  
2、drl文件中定义的函数，在when中也可以进行调用，需要使用eval(drl函数())来进行调用， 不过使用eval当多条件时会带来好多问题。eval虽然可以使用，但是性能不好，而且eval每次都会执行 eval 后面跟and和or条件时可能结果还不一样
3、在when中调用自定义function 
4、在drl文件中可以引入java的静态方法。 import 类名 ， 类名.静态方法。

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124773740](https://blog.csdn.net/fu_huo_1993/article/details/124773740)

# 参考链接

1、[eval性能不好,应该避免使用](https://stackoverflow.com/questions/17487725/how-much-of-a-performance-hit-does-eval-cause-in-drools)  
2、[eval如果要用的则正确的用法](https://blog.csdn.net/devotedwife/article/details/105716365)