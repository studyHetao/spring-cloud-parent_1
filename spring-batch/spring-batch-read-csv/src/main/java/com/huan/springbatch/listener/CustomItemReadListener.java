package com.huan.springbatch.listener;

import com.huan.springbatch.dto.Person;
import org.springframework.batch.core.ItemReadListener;

/**
 * 该接口用于对Reader相关的事件进行监控
 *
 * @author huan.fu
 * @date 2022/8/29 - 23:00
 */
public class CustomItemReadListener implements ItemReadListener<Person> {
    @Override
    public void beforeRead() {
        // beforeRead在每次Reader调用之前被调用
    }

    @Override
    public void afterRead(Person person) {
        // afterRead在每次Reader成功返回之后被调用
    }

    @Override
    public void onReadError(Exception ex) {
        // 而onReadError会在出现异常之后被调用，可以将其用于记录异常日志。
    }
}
