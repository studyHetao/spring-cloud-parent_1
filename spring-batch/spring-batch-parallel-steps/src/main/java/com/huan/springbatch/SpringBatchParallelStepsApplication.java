package com.huan.springbatch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan
 * <p>
 * `@EnableBatchProcessing`表示自动开启Spring Batch的自动配置
 * 并行Steps
 */
@SpringBootApplication
@EnableBatchProcessing
public class SpringBatchParallelStepsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchParallelStepsApplication.class, args);
    }
}
