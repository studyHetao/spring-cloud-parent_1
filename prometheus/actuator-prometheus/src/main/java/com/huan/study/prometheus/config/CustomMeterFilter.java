package com.huan.study.prometheus.config;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.config.MeterFilterReply;
import org.springframework.context.annotation.Configuration;

/**
 * @author huan.fu 2021/3/14 - 下午11:28
 */
@Configuration
public class CustomMeterFilter implements MeterFilter {
    /**
     * 只需要和jvm和order开头相关的指标数据，其余的数据一律拒绝
     */
    @Override
    public MeterFilterReply accept(Meter.Id id) {
        if (id.getName().startsWith("jvm") || id.getName().startsWith("order")) {
            return MeterFilterReply.ACCEPT;
        }
        return MeterFilterReply.DENY;
    }
}
