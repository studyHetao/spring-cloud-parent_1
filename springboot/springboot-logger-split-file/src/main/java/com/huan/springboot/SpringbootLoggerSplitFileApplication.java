package com.huan.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLoggerSplitFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootLoggerSplitFileApplication.class, args);
	}

}
