package com.huan.study.task.jobs;

import org.springframework.scheduling.support.CronExpression;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author huan.fu 2021/7/4 - 下午12:46
 */
public class CronNextExecTime {

    public static void main(String[] args) {
        CronNextExecTime.newMethod();
    }

    private static void oldMethod() {
        boolean validExpression = CronSequenceGenerator.isValidExpression("*/1 * * * * ?");
        System.out.println(validExpression);

        CronSequenceGenerator generator = new CronSequenceGenerator("0 10 8 * * ?");
        Date nextExecTime = generator.next(new Date());
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(nextExecTime));
    }

    private static void newMethod() {
        boolean validExpression = CronExpression.isValidExpression("*/1 * * * * ?");
        System.out.println(validExpression);

        CronExpression expression = CronExpression.parse("0 10 8 * * ?");
        LocalDateTime nextExecTime = expression.next(LocalDateTime.now());
        if (null != nextExecTime) {
            System.out.println(nextExecTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        }
    }
}
