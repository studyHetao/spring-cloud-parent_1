package com.huan.seata.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eureka server
 *
 * @author huan
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer50011Application {
    
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer50011Application.class, args);
    }
}
