package com.huan.xss.config;

import com.huan.xss.core.DefaultXssCleaner;
import com.huan.xss.core.JacksonXssClean;
import com.huan.xss.core.XssCleaner;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * xss 配置类
 *
 * @author huan.fu
 * @date 2022/10/24 - 20:54
 */
@Configuration
@RequiredArgsConstructor
public class XssConfiguration {

    private final XssProperties xssProperties;

    @Bean
    public XssCleaner xssCleaner() {
        return new DefaultXssCleaner(xssProperties);
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer xssJacksonCustomizer(XssCleaner xssCleaner) {
        return builder -> builder.deserializerByType(String.class, new JacksonXssClean(xssCleaner));
    }
}
