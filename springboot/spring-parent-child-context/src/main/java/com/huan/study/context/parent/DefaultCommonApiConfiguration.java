package com.huan.study.context.parent;

import com.huan.study.context.CommonApi;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huan.fu 2021/6/8 - 下午2:59
 */
@Configuration
public class DefaultCommonApiConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public CommonApi defaultCommonApi() {
        return new DefaultCommonApi();
    }
}
