package com.huan.hadoop.mr;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * reduce 阶段， key 必须是 DBWritable 类型, value随意
 *
 * @author huan.fu
 * @date 2023/7/16 - 11:07
 */
public class WriteDbReducer extends Reducer<NullWritable, Student, Student, NullWritable> {

    @Override
    protected void reduce(NullWritable key, Iterable<Student> values, Reducer<NullWritable, Student, Student, NullWritable>.Context context) throws IOException, InterruptedException {
        for (Student value : values) {
            context.write(value, NullWritable.get());
        }
    }
}
