package com.huan.ohc;

import org.caffinitas.ohc.OHCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 堆外内存的使用
 * <a href="https://github.com/snazy/ohc">ohc</a>
 * <p>  
 * 可能有些人会说，我用堆内内存就可以了，为啥还要搞个堆外内存？ 通常情况下，堆内内存就可以满足，但是如果 堆里的某个对象是个大对象
 * 且常驻内存的话，此时使用堆外内存就比堆内内存要好。
 *
 * @author huan.fu
 * @date 2023/5/16 - 20:04
 */
@SpringBootApplication
public class OhcApplication implements ApplicationRunner {

    private static final Logger log = LoggerFactory.getLogger(OhcApplication.class);

    @Resource
    private OHCache<String, String> ohCache;

    public static void main(String[] args) {
        SpringApplication.run(OhcApplication.class, args);


    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ohCache.put("堆外缓存", "往堆外缓存中放入数据");
        log.error("从堆外缓存中获取值: {}", ohCache.get("堆外缓存"));
        log.info("睡眠5s，此时堆外内存中的内存会过期.");
        TimeUnit.SECONDS.sleep(5);
        log.error("5s后，再次从堆外缓存中获取值: {}", ohCache.get("堆外缓存"));

    }
}
