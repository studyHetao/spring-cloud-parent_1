package com.huan.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author huan.fu 2021/9/16 - 下午2:54
 */
@SpringBootApplication
@MapperScan("com.huan.seata.mapper")
@EnableFeignClients(basePackages = "com.huan.seata.client")
public class OrderService50045Application {
    
    public static void main(String[] args) {
        SpringApplication.run(OrderService50045Application.class);
    }
}
