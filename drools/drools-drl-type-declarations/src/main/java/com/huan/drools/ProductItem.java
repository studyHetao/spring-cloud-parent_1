package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 订单项
 *
 * @author huan.fu
 * @date 2022/5/17 - 09:30
 */
@Getter
@Setter
public class ProductItem {
    /**
     * itemId
     */
    private String itemName;
}
