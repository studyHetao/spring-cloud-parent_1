package com.huan.seata.service;

/**
 * @author huan.fu 2021/9/27 - 下午2:01
 */
public interface AccountService {
    /**
     * 扣除用户{id}多少钱{amount}
     *
     * @param id     用户id
     * @param amount 需要扣除的钱
     */
    void debit(Integer id, Long amount);
}
