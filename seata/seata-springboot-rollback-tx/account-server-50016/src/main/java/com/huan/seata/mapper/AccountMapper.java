package com.huan.seata.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author huan.fu 2021/9/16 - 下午2:01
 */
public interface AccountMapper {
    int debit(@Param("id") Integer id, @Param("amount") Long amount);
}
