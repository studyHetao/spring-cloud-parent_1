package com.huan.drools.entrypoint;

import com.huan.drools.Api;
import org.drools.core.event.DebugProcessEventListener;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.EntryPoint;
import org.kie.api.runtime.rule.Match;

import java.util.Objects;

/**
 * drools 测试类
 */
public class DroolsEntryPointApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());
        kieSession.addEventListener(new DebugAgendaEventListener());
        kieSession.addEventListener(new DebugProcessEventListener());

        Api api = new Api("/users/info", 100);
        // 将此处的 api 实体类插入到 first-entry-point 这个EntryPoint上
        EntryPoint entryPoint = kieSession.getEntryPoint("first-entry-point");
        entryPoint.insert(api);

        kieSession.fireAllRules();

        kieSession.dispose();
    }
}
