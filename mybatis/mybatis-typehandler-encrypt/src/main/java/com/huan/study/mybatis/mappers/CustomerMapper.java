package com.huan.study.mybatis.mappers;

import com.huan.study.mybatis.entity.Customer;
import com.huan.study.mybatis.typehandler.Encrypt;
import org.apache.ibatis.annotations.Param;

/**
 * @author huan.fu 2021/5/18 - 上午10:43
 */
public interface CustomerMapper {

    int addCustomer(@Param("phone") Encrypt phone, @Param("address") String address);

    Customer findCustomer(@Param("phone") Encrypt phone);
}
