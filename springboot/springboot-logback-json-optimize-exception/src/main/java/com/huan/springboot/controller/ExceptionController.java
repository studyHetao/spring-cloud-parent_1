package com.huan.springboot.controller;

import com.huan.springboot.service.ExceptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 测试异常
 *
 * @author huan.fu
 * @date 2022/5/6 - 15:56
 */
@RestController
public class ExceptionController {
    private static final Logger log = LoggerFactory.getLogger(ExceptionController.class);
    @Resource
    private ExceptionService exceptionService;

    @GetMapping("exception/{num}")
    public String exception(@PathVariable("num") int num) {
        try {
            MDC.put("num", String.valueOf(num));
            exceptionService.exception(num);
        } catch (Exception e) {
            log.error("控制层捕获异常", e);
        }
        return "ok";
    }
}
