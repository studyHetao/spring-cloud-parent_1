package com.huan.springboot.websocket;

import org.yeauty.pojo.Session;

import java.util.List;

/**
 * @author huan.fu
 * @date 2022/4/26 - 14:16
 */
public interface WebsocketSessionRepository {

    void addSession(Session session);

    List<String> fetchAllUserIds();

    List<Session> findSession(String userId);

    List<Session> findAllSessions();
}
