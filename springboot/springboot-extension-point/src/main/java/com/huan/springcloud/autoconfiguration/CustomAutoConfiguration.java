package com.huan.springcloud.autoconfiguration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置类
 *
 * @author huan.fu
 * @date 2022/5/30 - 16:34
 */
@Configuration
public class CustomAutoConfiguration {

    @ConditionalOnMissingBean
    @Bean
    public AutoService autoService() {
        return new AutoService();
    }
}
