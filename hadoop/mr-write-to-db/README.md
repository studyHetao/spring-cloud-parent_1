# 1、需求

读取本地文件，并写入到数据库中

# 2、实现步骤

1. 编写一个实体类 `Student` 实现 `DBWritable`和`Writable` 接口
2. 编写驱动类

```java
// 配置当前作业需要的JDBC密码
DBConfiguration.configureDB(
        configuration,
        "com.mysql.cj.jdbc.Driver",
        "jdbc:mysql://localhost:3306/temp_work",
        "root",
        "root@1993"
);

// 配置本例子中的输入数据路径和输出数据路径，默认输入输出组件为： TextInputFormat和TextOutputFormat
FileInputFormat.setInputPaths(job, new Path("/tmp/mr-read-from-db/part-m-00000"));

// 设置输出格式
job.setOutputFormatClass(DBOutputFormat.class);
DBOutputFormat.setOutput(job, "student", "student_id", "student_name");

```

# 3、输出文件内容

```shell
➜  mr-read-from-db pwd
/tmp/mr-read-from-db
➜  mr-read-from-db ll
total 8
-rw-r--r--  1 huan  wheel    18B  7 16 10:43 part-m-00000
➜  mr-read-from-db cat part-m-00000
1	张三
2	李四
➜  mr-read-from-db
```