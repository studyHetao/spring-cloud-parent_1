package com.huan.drools.insertlogicalmethod;

import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools insert logical方法
 * 1. `insertLogical`可以向工作内存中插入`Fact`对象。
 * 2. `insertLogical`方法调用后，会导致模式的重新匹配，导致之前不会执行的规则，重新执行。
 * 3. `insertLogical`方法插入到工作内存的对象，在规则不成立时，会自动删除注意和`insert`的区别
 */
public class DroolsInsertLogicalApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        kieSession.insert(new Fire());
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("insertLogical_"));

        kieSession.dispose();
    }
}
