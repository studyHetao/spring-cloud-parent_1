package com.huan.cola.customer.service.impl;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import com.huan.cola.customer.api.CustomerService;
import com.huan.cola.customer.dto.cmd.CustomerAddCmd;
import com.huan.cola.customer.dto.cmd.CustomerUpdateCmd;
import com.huan.cola.customer.dto.data.CustomerDTO;
import com.huan.cola.customer.dto.qry.GetCustomerByPhoneQry;
import com.huan.cola.customer.executor.CustomerAddCmdExe;
import com.huan.cola.customer.executor.query.FindAllQryExe;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 客户service 接口实现
 *
 * @author huan.fu
 * @date 2022/6/7 - 10:16
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Resource
    private FindAllQryExe findAllQryExe;
    @Resource
    private CustomerAddCmdExe customerAddCmdExe;

    @Override
    public MultiResponse<CustomerDTO> findAll() {
        return findAllQryExe.execute();
    }

    @Override
    public SingleResponse<CustomerDTO> getCustomer(GetCustomerByPhoneQry getCustomerByPhoneQry) {
        return null;
    }

    @Override
    public SingleResponse<Boolean> addCustomer(CustomerAddCmd customerAddCmd) {
        return customerAddCmdExe.execute(customerAddCmd);
    }

    @Override
    public SingleResponse<Boolean> updateCustomer(CustomerUpdateCmd customerUpdateCmd) {
        return null;
    }

    @Override
    public SingleResponse<Boolean> deleteCustomer(String phone) {
        return null;
    }
}
