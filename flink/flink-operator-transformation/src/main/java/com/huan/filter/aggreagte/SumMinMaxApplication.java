package com.huan.filter.aggreagte;

import com.huan.filter.vo.Person;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * sum()：在输入流上，对指定的字段做叠加求和的操作。
 * min()：在输入流上，对指定的字段求最小值。
 * max()：在输入流上，对指定的字段求最大值。
 * minBy()：与min()类似，在输入流上针对指定字段求最小值。不同的是，min()只计算指定字段的最小值，其他字段会保留最初第一个数据的值；而minBy()则会返回包含字段最小值的整条数据。
 * maxBy()：与max()类似，在输入流上针对指定字段求最大值。两者区别与min()/minBy()完全一致。
 *
 * @author huan.fu
 * @date 2023/9/22 - 23:02
 */
public class SumMinMaxApplication {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        environment.fromElements(
                        new Person(1, "张三", 20),
                        new Person(1, "李四", 10),
                        new Person(2, "王五", 25),
                        new Person(2, "赵六", 55),
                        new Person(5, "田七", 30)
                )
                .keyBy(Person::getId)
                // keyBy后的一组数据 根据 sum 求和
                .sum("age")
                // .max("age")
                // .min("age")
                // .maxBy("age")
                // .minBy("age")
                .print();

        environment.execute("max sum min operation");
    }
}
