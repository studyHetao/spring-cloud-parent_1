package com.huan.hadoop.mr;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * reduce操作， value因为不需要，直接使用NullWritable
 *
 * @author huan.fu
 * @date 2023/7/12 - 21:04
 */
public class PartitionReducer extends Reducer<Text, Text, Text, NullWritable> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, NullWritable>.Context context) throws IOException, InterruptedException {
        // 直接写出
        for (Text value : values) {
            context.write(value, NullWritable.get());
        }
    }
}
