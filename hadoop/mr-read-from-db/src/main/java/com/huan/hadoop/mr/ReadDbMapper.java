package com.huan.hadoop.mr;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * 处理从数据库中读取出来的每行数据， 输出的key不用，输出的value为每行数据
 *
 * @author huan.fu
 * @date 2023/7/16 - 10:23
 */
@Slf4j
public class ReadDbMapper extends Mapper<LongWritable, Student, NullWritable, Text> {

    private final Text outValue = new Text();

    @Override
    protected void map(LongWritable key, Student value, Mapper<LongWritable, Student, NullWritable, Text>.Context context) throws IOException, InterruptedException {
        log.info("开始处理第 {} 个数据.", key.get() + 1);
        outValue.set(value.toString());
        context.write(NullWritable.get(), outValue);
    }
}
