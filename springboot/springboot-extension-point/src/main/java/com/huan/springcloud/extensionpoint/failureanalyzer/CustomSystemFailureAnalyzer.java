package com.huan.springcloud.extensionpoint.failureanalyzer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

/**
 * 将程序启动过程中发生的异常翻译成可读的格式
 */
public class CustomSystemFailureAnalyzer extends AbstractFailureAnalyzer<Throwable> {

    private static final Logger log = LoggerFactory.getLogger(CustomSystemFailureAnalyzer.class);

    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, Throwable cause) {
        log.info("发生了异常", rootFailure);

        return new FailureAnalysis("这是异常描述", "发生阶段", rootFailure);
    }
}
