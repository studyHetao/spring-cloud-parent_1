package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * 无状态session测试
 *
 * @author huan.fu
 * @date 2022/5/13 - 10:27
 */
public class StatelessSessionApplication {

    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieClasspathContainer = kieServices.getKieClasspathContainer();
        StatelessKieSession session = kieClasspathContainer.newStatelessKieSession("stateless-session");

        // 设置一个session级别的全局变量
        session.setGlobal("username", "huan.fu");

        Counter counter01 = new Counter("count-01", 0);
        // 第一调用execute方法
        session.execute(counter01);

        List<Command<?>> commands = new ArrayList<>();
        Counter counter02 = new Counter("count-02", 0);

        commands.add(CommandFactory.newInsert(counter02));
        BatchExecutionCommand batchExecutionCommand = CommandFactory.newBatchExecution(commands);
        // 第二次调用execute方法
        session.execute(batchExecutionCommand);
    }
}
