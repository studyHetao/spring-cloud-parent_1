package com.huan.springcloud.user;

import com.huan.springcloud.common.CommonApi;
import com.huan.springcloud.product.ProductApi;
import org.springframework.context.annotation.Bean;

/**
 * 不可使用 @Configuration 注解
 *
 * @author huan.fu
 * @date 2023/8/15 - 10:05
 */
public class UserApiConfig {

    @Bean
    public CommonApi commonApi() {
        return new UserApi();
    }
}
