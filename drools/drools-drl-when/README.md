# 实现功能

drools中when阶段各种条件的用法

# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/124928724](https://blog.csdn.net/fu_huo_1993/article/details/124928724)

# 参考链接

[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-rules-WHEN-con_drl-rules](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-rules-WHEN-con_drl-rules)