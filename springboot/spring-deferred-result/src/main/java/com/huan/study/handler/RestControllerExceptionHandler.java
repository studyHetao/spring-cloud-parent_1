package com.huan.study.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author huan.fu 2021/10/14 - 下午1:43
 */
@RestControllerAdvice
public class RestControllerExceptionHandler {
    
    private static final Logger log = LoggerFactory.getLogger(RestControllerExceptionHandler.class);
    
    @ExceptionHandler(Exception.class)
    public String exception(Exception exception) {
        log.error("出现了异常", exception);
        return "出现了异常，统一处理";
    }
}
