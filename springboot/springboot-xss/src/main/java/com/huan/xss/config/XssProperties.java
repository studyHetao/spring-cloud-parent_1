package com.huan.xss.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * xss 配置
 * @author huan.fu
 * @date 2022/10/24 - 20:26
 */
@Getter
@Setter
@ConfigurationProperties("security.xss")
public class XssProperties {

    private boolean enabled;

}
