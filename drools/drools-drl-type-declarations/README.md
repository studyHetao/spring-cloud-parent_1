# 实现功能

1、在drl文件中声明一个枚举类型。    
2、在drl文件中声明一个类。  
3、在drl文件中声明一个类并完成继承操作。  
4、编写`rule`并使用我们自定义的type。  
5、java中给在drl文件中声明的type赋值，包括类和枚举类型

# 2、java代码中获取drl声明的类型
## 1、非枚举类型
```java
KieBase kieBase = kieContainer.getKieBase("type-kabse");
												// 规则文件的包名  声明的类型名
FactType productOrderFactType = kieBase.getFactType("rules", "ProductOrder");
Object instance = productOrderFactType.newInstance();
productOrderFactType.set(instance, "orderId", 20220517121212001L);
```
## 2、获取枚举类型的值
`需要通过反射来获取到。`

```java
KieBase kieBase = kieContainer.getKieBase("type-kabse");
// 此处的FactType的真实类型是EnumClassDefinition,可以获取到枚举中构造方法的参数的值
FactType orderStatusFactType = kieBase.getFactType("rules", "OrderStatus");

// 获取drools中枚举的值
Class<?> factClass = orderStatusFactType.getFactClass();
Method method = factClass.getMethod("valueOf", String.class);
Object pay = method.invoke(null, "PAY");
```

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124726468](https://blog.csdn.net/fu_huo_1993/article/details/124726468)

# 参考链接
[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-declarations-con_drl-rules](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#drl-declarations-con_drl-rules)