package com.huan.study.pointcut.configuration;

import com.huan.study.pointcut.aop.LogPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置
 *
 * @author huan.fu 2021/6/10 - 下午2:26
 */
@Configuration
public class LogConfiguration {

    @Bean
    public LogPostProcessor logPostProcessor() {
        LogPostProcessor logPostProcessor = new LogPostProcessor();
        // 设置执行的顺序
        logPostProcessor.setOrder(0);
        return logPostProcessor;
    }
}
