package com.huan.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author huan.fu 2021/9/16 - 下午2:02
 */
@SpringBootApplication
@MapperScan("com.huan.seata.mapper")
public class AccountService50001Application {
    
    public static void main(String[] args) {
        SpringApplication.run(AccountService50001Application.class);
    }
}
