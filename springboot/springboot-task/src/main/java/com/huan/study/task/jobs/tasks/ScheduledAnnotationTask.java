package com.huan.study.task.jobs.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 使用注解
 *
 * @author huan.fu 2021/7/8 - 下午3:14
 */
@Component
@Slf4j
public class ScheduledAnnotationTask {

    @Scheduled(cron = "* * * * * ?")
    public void scheduledAnnotationTask() {
        log.info("我是使用注解实现的定时任务");
    }
}
