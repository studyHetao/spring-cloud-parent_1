package com.huan.seata.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author huan.fu 2021/9/16 - 下午1:57
 */
@Getter
@Setter
@ToString
public class AccountDO {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 姓名
     */
    private String name;
    /**
     * 余额，单位分
     */
    private Long balance;
}
