package com.huan.seata.service;

import java.util.Date;

/**
 * @author huan.fu 2021/9/16 - 下午2:01
 */
public interface AccountService {
    /**
     * 扣除用户{id}多少钱{amount}
     *
     * @param id         用户id
     * @param amount     需要扣除的钱
     * @param updateTime 修改时间
     */
    void debit(Integer id, Long amount, Date updateTime);
}
