package com.huan.springbatch.listener;

import com.huan.springbatch.dto.Person;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

/**
 * ItemWriteListener的功能和ItemReadListener、ItemReadListener类似，但是需要注意的是它接收和处理的数据对象是一个List。List的长度与chunk配置相关。
 *
 * @author huan.fu
 * @date 2022/8/29 - 23:03
 */
public class CustomItemWriteListener implements ItemWriteListener<Person> {
    @Override
    public void beforeWrite(List<? extends Person> items) {

    }

    @Override
    public void afterWrite(List<? extends Person> items) {

    }

    @Override
    public void onWriteError(Exception exception, List<? extends Person> items) {

    }
}
