# 一、介绍

1. 此处不使用SpringBoot官方提供的websocket实现，使用第三方实现 netty-websocket-spring-boot-starter
2. 此处的websocket是使用netty来实现的
3. 第三方jar包的地址为: https://gitee.com/Yeauty/netty-websocket-spring-boot-starter

# 二、实现步骤

## 1、引入依赖

```xml

<dependency>
    <groupId>org.yeauty</groupId>
    <artifactId>netty-websocket-spring-boot-starter</artifactId>
    <version>0.12.0</version>
</dependency>
```

## 2、配置ServerEndpointExporter对象

```java

@Configuration
public class WebsocketConfig {

    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
```

## 3、编码实现

```java

@ServerEndpoint(path = "${ws.path}", host = "${ws.host}", port = "${ws.port}",
        bossLoopGroupThreads = "${ws.boss-loop-group-threads}", workerLoopGroupThreads = "${ws.worker-loop-group-threads}")
public class WebsocketHandler {
    private static final Logger log = LoggerFactory.getLogger(WebsocketHandler.class);

    @BeforeHandshake
    public void handshake(Session session, HttpHeaders headers, @RequestParam String req, @RequestParam MultiValueMap reqMap) {
    }

    @OnOpen
    public void onOpen(Session session, HttpHeaders headers, @RequestParam String req, @RequestParam MultiValueMap reqMap) {
        log.info("连接打开session:{}", session.hashCode());
    }

    @OnClose
    public void onClose(Session session) {
        log.info("连接关闭session:{}", session.hashCode());
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        log.info("发生了错误", throwable);
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        log.info("session:{} 收到了消息: {}", session.hashCode(), message);
    }

    @OnBinary
    public void onBinary(Session session, byte[] bytes) {
        log.info("获取到了二进制消息");
    }

    @OnEvent
    public void onEvent(Session session, Object evt) {
        log.info("接收到了事件");
    }
}
```

需要注意上方的注解都是 `org.yeauty.annotation`包下的，比如：`@OnOpen`的全路径为 `com.huan.springboot.websocket.WebsocketHandler.onOpen`

# 三、注意事项

1. 端点类上的注解是`org.yeauty.annotation`包下的，比如`@OnOpen`、`@OnMessage`等
2. 如果要获取路径上的参数，比如'/ws/{userId}' 可以使用`@PathVariable`来获取
3. websocket的端口和server的端口是不一致的，比如这个例子中，websocket的端口是`9092`，而server的端口是`9091`

# 四、参考网址

1、[https://gitee.com/Yeauty/netty-websocket-spring-boot-starter](https://gitee.com/Yeauty/netty-websocket-spring-boot-starter)