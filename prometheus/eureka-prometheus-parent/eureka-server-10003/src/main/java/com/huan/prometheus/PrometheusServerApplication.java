package com.huan.prometheus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author huan.fu 2021/3/15 - 下午11:18
 */
@SpringBootApplication
@EnableEurekaServer
public class PrometheusServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrometheusServerApplication.class, args);
    }
}
