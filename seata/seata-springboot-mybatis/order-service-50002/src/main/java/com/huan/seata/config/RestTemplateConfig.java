package com.huan.seata.config;

import com.huan.seata.interceprot.SeataRestTemplateInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate 配置
 *
 * @author huan.fu 2021/9/24 - 上午9:29
 */
@Configuration
public class RestTemplateConfig {
    
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        // 添加 seata 拦截器，传递 xid 的值
        restTemplate.getInterceptors().add(new SeataRestTemplateInterceptor());
        return restTemplate;
    }
}
