package com.huan.cola.customer.domain.ability;

import com.huan.cola.customer.domain.model.Customer;

/**
 * 客户领域服务
 *
 * @author huan.fu
 * @date 2022/6/7 - 17:07
 */
public interface CustomerDomainService {

    /**
     * 检测手机号是否存在
     */
    boolean checkPhoneExists(String phone);

    /**
     * 创建客户
     */
    boolean create(Customer customer);
}
