package com.huan.drools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * drools 集成 springboot
 */
@SpringBootApplication
public class DroolsIntegratedSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(DroolsIntegratedSpringBootApplication.class, args);
    }
}
