```
├─springboot-extension-point
├───failureanalyzer
│        >> 将程序在启动阶段发生的异常翻译成可读形式
├───environmentpostprocessor [博客](https://blog.csdn.net/fu_huo_1993/article/details/124396988)
│        >> EnvironmentPostProcessor 该扩展点是在 ApplicationContext 刷新之前执行，可以让我们自定义环境（Environment）中的值
│        >> 此处我们完成我们自定义的属性覆盖掉默认配置文件中的属性
│        >> 日志无法输出的解决办法
│        >> org.springframework.boot.context.logging.LoggingApplicationListener 初始化日志系统
│        >> 可以实现配置的加解密，此处没有实现
├───autoconfiguration
│        >> 自动配置的类需要加载@Configuration注解
│        >> 在spring.factories中配置org.springframework.boot.autoconfigure.EnableAutoConfiguration=自动配置的类
├───beandefinitionregistrypostprocessor
│        >> 系统中的BeanDefinition都加载完了，但是没有进行实例化操作
│        >> 我们可以自己在此增加BeaDefition或者修改已经存在的BeanDefition对象
├───beanfactorypostprocessor
│        >> 此对象是在BeanDefinitionRegistryPostProcessor对象之后执行，此处所有的BeanDefition也是还没有实例化的。
├───beanpostprocessor
│        >> 对Bean调用初始化方法前后的处理。
│        >> 初始化方法（init-method、@PostConstruct、afterPropertiesSet）

```