# 疑问

我们知道在`Spring Cloud OpenFiegn`中，可以为每个客户端进行自定义各种配置，在`Spring Cloud Loadbalancer`中也可以为
各个客户端进行自定义配置，那么这个是什么实现的呢？

# 解决方案

实现的思路就是创建`父子上下文`来实现，`子上下文`可以访问父上下文，但是`父上下文`则不可访问子上下文。

`实现方式`

1. 自己创建父子上下文来实现。 [spring-parent-child-context](../spring-parent-child-context)
2. 通过Spring Cloud提供的 NamedContextFactory 来实现。

# 案例

```text
    CommonApi（父上下文实现：默认的api接口）
        DefaultCommonApi（默认实现-父或子上下文默认实现）
        ProductApi(子上下文实现)
        UserApi(子上下文实现)    
        
    ChildClientNamedContextFactory(父子上下文类)
    ClientSpecification（父上下文中： 每个客户端的自定义配置， name=客户端的名字，configuration=具体的配置，需要在子上下文中进行实现，因此configuration指定的类上不可增加@Configuration注解）
        ClientSpecification： 注册在父上下文中
        ClientSpecification： 这个类里面的configuration对应的Class类，注册在子上下文中
    ChildClientNamedContextFactoryConfig（父上下文中： 将ChildClientNamedContextFactory注册到父上下文中）
    ClientSpecificationRegister（父上下文中，将ClientSpecification注册到父上下文中）
    DefaultCommonApiConfig
        - 父上下文中的默认实现
        - 如果子上下文中有实现则会使用子上下文的，否则使用父上下文的。
```

# 类图

![父子上下文.png](父子上下文.png)