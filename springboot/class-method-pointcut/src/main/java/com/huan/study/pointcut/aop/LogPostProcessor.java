package com.huan.study.pointcut.aop;

import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.autoproxy.AbstractBeanFactoryAwareAdvisingPostProcessor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author huan.fu 2021/6/10 - 下午2:15
 */
public class LogPostProcessor extends AbstractBeanFactoryAwareAdvisingPostProcessor implements InitializingBean {

    @Override
    public void afterPropertiesSet() {
        // 切入点
        // 类上和方法上都必须要存在 @Log 注解，才会被拦截到，如果只需要类上有或方法上有，则可以删除其中的一个
        Pointcut pointcut = new AnnotationMatchingPointcut(Log.class, Log.class, true);

        /**
         * 连接2个pointcut
         * ComposablePointcut composablePointcut = new ComposablePointcut(pointcut)
         * .union(Pointcut anotherPointcut);
         */

        this.advisor = new DefaultPointcutAdvisor(pointcut, new LogInterceptor());
    }

    @Override
    public void setOrder(int order) {
        super.setOrder(order);
    }
}
