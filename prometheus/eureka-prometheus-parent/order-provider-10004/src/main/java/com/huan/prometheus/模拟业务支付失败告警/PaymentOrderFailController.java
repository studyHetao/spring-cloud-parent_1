package com.huan.prometheus.模拟业务支付失败告警;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huan.fu 2021/3/24 - 下午2:39
 */
@RestController
public class PaymentOrderFailController {

    @Autowired
    private MeterRegistry meterRegistry;

    /**
     * 产生告警
     */
    @GetMapping("orderPaymentFail")
    public String orderPaymentFail(@RequestParam("orderId") String orderId,
                                   @RequestParam("level") String level) {
        final long alertTime = System.currentTimeMillis() / 1000;
        // 对于同一个时间序列，指标名称+标签+标签值 要一致，如果不一致则可能产生一个新的。
        // 比如：如果想记录发生的时间、分布式追踪id 等，那么则可能每次都会生成一个新的时间序列。
        Gauge.builder("payment_fail_order", () -> alertTime)
                .tag("module", "order")
                .tag("orderId", orderId)
                // 不给时间，不然相同的订单每次产生的告警信息都不一样
                // .tag("time", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                .tag("level", level)
                .description("描述")
                .register(meterRegistry);
        return "产生一个告警";
    }
}
