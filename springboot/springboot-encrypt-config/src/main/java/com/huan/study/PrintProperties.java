package com.huan.study;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 打印配置
 *
 * @author huan.fu 2021/10/18 - 下午2:16
 */
@Component
public class PrintProperties implements InitializingBean {
    
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final Logger log = LoggerFactory.getLogger(PrintProperties.class);
    
    @Autowired
    private DataSourceProperties dataSourceProperties;
    @Autowired
    private StringEncryptor stringEncryptor;
    @Autowired
    private DataSource dataSource;
    
    @Override
    public void afterPropertiesSet() throws SQLException {
        // 输出加密的属性值
        System.out.println(stringEncryptor.encrypt("jdbc:mysql://127.0.0.1:3306/seata_account?useUnicode=true&characterEncoding=utf8&autoReconnectForPools=true&useSSL=false"));
        // 获取一个数据库连接
        System.out.println(dataSource.getConnection());
        log.warn("=================================(start)=================================");
        log.warn("url:[{}]", dataSourceProperties.getUrl());
        log.warn("driver-class-name:[{}]", dataSourceProperties.getDriverClassName());
        log.warn("username:[{}]", dataSourceProperties.getUsername());
        log.warn("password:[{}]", dataSourceProperties.getPassword());
        log.warn("=================================(end)=================================");
    }
}
