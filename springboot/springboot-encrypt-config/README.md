# 加密SpringBoot的配置

## 实现步骤

### 1、引入jar包

```xml
<dependency>
    <groupId>com.github.ulisesbocchio</groupId>
    <artifactId>jasypt-spring-boot-starter</artifactId>
    <version>3.0.4</version>
</dependency>
```

### 2、java代码加密

```java
package com.huan.study;

import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 打印配置
 *
 * @author huan.fu 2021/10/18 - 下午2:16
 */
@Component
public class PrintProperties implements InitializingBean {
    
    private static final Logger log = LoggerFactory.getLogger(PrintProperties.class);
    
    @Autowired
    private StringEncryptor stringEncryptor;
    
    @Override
    public void afterPropertiesSet() throws SQLException {
        // 输出加密的属性值
        System.out.println(stringEncryptor.encrypt("jdbc:mysql://127.0.0.1:3306/seata_account?useUnicode=true&characterEncoding=utf8&autoReconnectForPools=true&useSSL=false"));
    }
}

```

### 3、配置文件中替换

```properties
spring.datasource.url=ENC(3U9evfRBn5DmlKOgND8fLVmjL0lftVubkzS0x6JOxhwr5sGOJwQ1BBn7NkgtbEHiePaym00MckPELTklrBRczrvdi7aZTMR9GZYJTP/DK+/Dlt7IhXdzVDFFuwUPcpDNVBQpD/z5hxRe1T6woiGFULX1haMn4ZXJeKST6RExDBEYopsT1j5SfEF3dLxAAGgPJ+ooo4AgutgBJdmwLIKdZQ==)
```

### 4、项目启动时配置加密的密钥

```
java -Djasypt.encryptor.password=123456789 -jar xxx.jar
```

## 参考地址

1. [https://github.com/ulisesbocchio/jasypt-spring-boot](https://github.com/ulisesbocchio/jasypt-spring-boot)