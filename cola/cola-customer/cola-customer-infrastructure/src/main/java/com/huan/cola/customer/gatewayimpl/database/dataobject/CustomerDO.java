package com.huan.cola.customer.gatewayimpl.database.dataobject;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 和数据库字段一一对应
 *
 * @author huan.fu
 * @date 2022/6/7 - 11:22
 */
@Getter
@Setter
@ToString
public class CustomerDO {
    private Integer id;
    private String phone;
    private String address;
    private String idCard;
    private Integer customerType;
}
