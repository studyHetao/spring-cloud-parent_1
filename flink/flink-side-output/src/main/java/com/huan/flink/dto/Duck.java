package com.huan.flink.dto;

/**
 * 鸭子
 *
 * @author huan.fu
 * @date 2023/9/23 - 09:57
 */
public class Duck extends Animal {
    public Duck() {
    }

    public Duck(String type, String name) {
        super(type, name);
    }

    @Override
    public String toString() {
        return "Duck{ type=" + this.getType() + ", name=" + this.getName() + " }";
    }
}
