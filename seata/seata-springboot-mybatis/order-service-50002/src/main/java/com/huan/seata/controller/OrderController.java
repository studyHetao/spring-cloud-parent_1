package com.huan.seata.controller;

import com.huan.seata.service.BusinessService;
import io.seata.core.context.RootContext;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huan.fu 2021/9/16 - 下午2:45
 */
@RestController
@RequiredArgsConstructor
public class OrderController {
    
    private final BusinessService businessService;
    
    @GetMapping("createOrder")
    public String createOrder(@RequestParam("accountId") Integer accountId, @RequestParam("amount") Long amount,
                              @RequestParam(value = "hasException", defaultValue = "false") boolean hasException) {
        System.out.println("createOrder:" + RootContext.getXID());
        businessService.createAccountOrder(accountId, amount, hasException);
        return "下单成功";
    }
}
