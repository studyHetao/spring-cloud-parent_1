package com.huan.study.controller;

import com.huan.study.entity.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * person controller
 *
 * @author huan.fu
 * @date 2023/1/14 - 23:46
 */
@RestController
public class PersonController {

    /**
     * 通过 @JsonSerialize 自定义序列化后的值
     *
     * @return Person
     */
    @GetMapping("person")
    public Person find() {
        Person person = new Person();
        person.setId(1);
        person.setOldId(0);
        person.setAge(10);
        person.setOldAge(0);
        person.setName("名字");
        person.setAddress(null);
        person.setMoney(BigDecimal.ZERO);
        person.setMoney(BigDecimal.valueOf(10));
        return person;
    }
}
