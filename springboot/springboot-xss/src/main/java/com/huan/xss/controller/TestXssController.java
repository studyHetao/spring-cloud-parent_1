package com.huan.xss.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 测试 xss 控制器
 *
 * @author huan.fu
 * @date 2022/10/24 - 21:00
 */
@RestController
@RequestMapping("/xss")
@Slf4j
public class TestXssController {

    @GetMapping("formXss1")
    public String formXss1(String username, @RequestParam("password") String password) {
        log.info("formXss1: username: {} password: {}", username, password);
        return "username:" + username + " password:" + password;
    }

    @PostMapping("formXss2")
    public String formXss2(User user) {
        log.info("formXss2: username: {} password: {}", user.username, user.password);

        return "username:" + user.username + " password:" + user.password;
    }

    @PostMapping("json1")
    public String json1(@RequestBody User user) {
        log.info("json1: username: {} password: {}", user.username, user.password);

        return "username:" + user.username + " password:" + user.password;
    }

    @PostMapping("json2")
    public User json2(@RequestBody User user) {
        log.info("json2: username: {} password: {}", user.username, user.password);
        return new User("张三", "<script>密码</script>");
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    public static class User {
        private String username;
        private String password;
    }
}
