package com.huan.drools.delete;

import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools delete方法(删除工作内存中的对象，retract也是删除工作内存中的对象，但是不推荐使用)
 */
public class DroolsDeleteApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        kieSession.insert(new Fire());
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("delete_"));

        kieSession.dispose();
    }
}
